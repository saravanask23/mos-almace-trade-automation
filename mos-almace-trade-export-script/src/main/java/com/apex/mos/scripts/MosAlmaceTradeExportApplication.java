package com.apex.mos.scripts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MosAlmaceTradeExportApplication {

	public static void main(String[] args) {
		SpringApplication.run(MosAlmaceTradeExportApplication.class, args);
	}

}
